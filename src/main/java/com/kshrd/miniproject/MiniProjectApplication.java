package com.kshrd.miniproject;

import com.kshrd.miniproject.service.Imp.FileStorageServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniProjectApplication {
    FileStorageServiceImp fileStorageServiceImple;
    public static void main(String[] args) {
        SpringApplication.run(MiniProjectApplication.class, args);
    }
    @Autowired
    public void setFileStorageServiceImple(FileStorageServiceImp fileStorageServiceImple) {
        this.fileStorageServiceImple = fileStorageServiceImple;
    }
}

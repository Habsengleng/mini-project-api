package com.kshrd.miniproject.configuration;

import com.kshrd.miniproject.model.Book;
import com.kshrd.miniproject.model.Category;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class ApplicationConfig implements RepositoryRestConfigurer {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Book.class, Category.class);
        config.setBasePath("api/v1");
    }
}

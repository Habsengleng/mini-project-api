package com.kshrd.miniproject.repository;

import com.kshrd.miniproject.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;


@RepositoryRestResource
public interface BookRepository extends JpaRepository<Book, Integer> {
    @RestResource(path = "/findTitle")
    Book findByTitleContainingIgnoreCase(@Param("title") String title);
}

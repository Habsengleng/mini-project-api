package com.kshrd.miniproject.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {
    public String saveThumbnail(MultipartFile file);
    public Resource load(String filename);
}

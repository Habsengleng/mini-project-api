package com.kshrd.miniproject.service.Imp;

import com.kshrd.miniproject.service.FileStorageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class FileStorageServiceImp implements FileStorageService {
    private final Path root = Paths.get("src/main/resources/images/");
    @Value("${file.upload.server.path}")
    private String serverPath;

    @Override
    public String saveThumbnail(MultipartFile file) {
        String fileDownloadUri = "";
        if(!file.isEmpty()) {
            fileDownloadUri = UUID.randomUUID() + "." + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath, fileDownloadUri));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return fileDownloadUri;
    }
    @Override
    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }
}
